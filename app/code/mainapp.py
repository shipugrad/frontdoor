from flask import Flask, request, json, current_app, jsonify, render_template
import os

app = Flask(__name__)

@app.route("/", methods=["GET"])

def index():
    env = os.environ['env']
    json_data = request.json
    headers = dict(request.headers)
    return render_template(
                'index.html',
                title="page",
                env=env,
                jsonfile=json.dumps(json_data),
                headerfile=json.dumps(headers)
            )

if __name__ == "__main__":
    app.run(host="0.0.0.0")
